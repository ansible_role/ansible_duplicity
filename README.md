## Duplicity ansible role.

This role is intended for installing and configuring 
[duplicity](http://duplicity.nongnu.org/) backup cron tasks for every weekday.

*It was tested for webdav, webdavs backends only, but it can suits for 
other backends too.*
